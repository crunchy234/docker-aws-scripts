require 'rake'

$LOAD_PATH << '.'
require 'docker'
include Docker

# Note: This AMI is assumed to be a fedora AMI see fedora cloud get the AMI for your region: https://alt.fedoraproject.org/cloud/
AMI = 'ami-03c1878774842a9ee'  # GP2 HMV AMI - Faster SSD storage
# AMI = 'ami-976ea4f5'  # Standard HVM AMI - Slower storage
INSTANCE_TYPE = 't2.medium'
AMI_USER_NAME = 'fedora'

def setup_container
  instance_url = File.read("instanceUrl-#{INSTANCE_TYPE}.txt").strip
  ssh_prefix = "ssh -oStrictHostKeyChecking=no -i ~/.ssh/aws-key.pem #{AMI_USER_NAME}@#{instance_url}"

  sh "#{ssh_prefix} \"sudo dnf clean all ; sudo dnf -y install dnf-plugins-core git ; sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo ; sudo dnf install -y docker-ce ; sudo groupadd docker; sudo usermod -aG docker #{AMI_USER_NAME} ; sudo systemctl enable docker ; sudo systemctl start docker\""
  # Setup login to registry if details present
  if defined? Docker::REGISTRY
    sh "#{ssh_prefix} \"docker login #{Docker::REGISTRY} -u #{Docker::USER} -p #{Docker::PASSWORD}\""
  end

  if defined? Docker::REPO
    repo_str = Docker::REPO.sub! 'https://', "https://#{Docker::USER}:#{Docker::PASSWORD}@"
    sh "#{ssh_prefix} \"git clone #{repo_str}\""
  end
end

desc 'Create aws instance'
task :create do
  sh "./setup_container.sh #{AMI} #{INSTANCE_TYPE}"
end

desc 'pull and run docker container defined in docker.rb on aws'
task :run do
  sh "./setup_container.sh #{AMI} #{INSTANCE_TYPE}"
  setup_container
end

desc 'setup docker on the new EC2 instance'
task :setup_container do
  setup_container
end

desc 'Run docker image'
task :docker do
  instance_url = File.read("instanceUrl-#{INSTANCE_TYPE}.txt").strip
  ssh_prefix = "ssh -oStrictHostKeyChecking=no -t -i ~/.ssh/aws-key.pem #{AMI_USER_NAME}@#{instance_url}"
  if defined? Docker::REPO
    sh "#{ssh_prefix} \"docker run --rm -tiv \\$(pwd):/home/working #{Docker::CONTAINER}\""

  else
    sh "#{ssh_prefix} \"docker run --rm -ti #{Docker::CONTAINER}\""
  end
end

desc 'pause EC2 and VPC'
task :pause do
  begin
  instance_url = File.read("instanceUrl-#{INSTANCE_TYPE}.txt").strip
  ssh_prefix = "ssh -oStrictHostKeyChecking=no -t -i ~/.ssh/aws-key.pem #{AMI_USER_NAME}@#{instance_url}"
  sh "#{ssh_prefix} \"sudo poweroff\""
  rescue
    puts 'Caught error during shutdown'
  end

end

desc 'resume EC2 and VPC'
task :resume do
  sh "./resumeCurrent.sh"
end

desc 'connect EC2 and VPC'
task :connect do
  sh "./connectCurrent.sh"
end

desc 'terminate EC2 and VPC'
task :terminate do
  sh "./terminateCurrent.sh"
end


