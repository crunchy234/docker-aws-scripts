# Docker AWS Scripts
This script requires a `docker.rb` file to be created in the repo root looking like the following:
```ruby
module Docker
  REGISTRY='docker.pkg.github.com'
  USER='my-awesome-user'
  PASSWORD='super-secret-password-here'
  REPO='https://github.com/user/repo.git'  # Note: This does not have to be supplied
  CONTAINER='ubuntu:18.04'
end
```

## Adding extra private keys to the instance
Add a file in the root of the repo called `extraPublicKeys.txt`.
This file should be formatted in the same way the `authorized_keys` file.
